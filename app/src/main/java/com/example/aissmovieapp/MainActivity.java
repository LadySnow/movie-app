package com.example.aissmovieapp;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.aissmovieapp.adapter.MovieAdapter;
import com.example.aissmovieapp.model.Movie;
import com.example.aissmovieapp.model.MoviesResponse;
import com.example.aissmovieapp.retrofit.ApiClient;
import com.example.aissmovieapp.retrofit.ApiService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    String apiKey = "43a7ea280d085bd0376e108680615c7f";

    private static final boolean GRID_LAYOUT = true;

    @BindView(R.id.sr_movies_list)
    SwipeRefreshLayout sr_movies;
    @BindView(R.id.rv_movies)
    RecyclerView rv_movies;
    @BindView(R.id.cardView_detailsMovie)
    CardView details;
    @BindView(R.id.close_details)
    ImageView iv_close_details;
    @BindView(R.id.poster)
    CircleImageView poster;
    @BindView(R.id.tv_title)
    TextView title;
    @BindView(R.id.tv_budget)
    TextView budget;
    @BindView(R.id.tv_vote)
    TextView vote;

    MovieAdapter adapter;
    List<Movie> movies=new ArrayList<>();
    SearchView searchView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (GRID_LAYOUT) {
            final int columns = 2;
            rv_movies.setLayoutManager(new GridLayoutManager(getApplicationContext(), columns));
        } else {
            rv_movies.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        }

        adapter = new MovieAdapter(movies, detailsMovie);

        rv_movies.setAdapter(adapter);
        getMoviesItems();
        iv_close_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                details.setVisibility(View.GONE);
            }
        });
    }

    private void getMoviesItems() {

        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<MoviesResponse> call = service.getMovies(apiKey);

        call.enqueue(new Callback<MoviesResponse>() {
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                if(response.body() != null) {
                    movies.clear();
                    movies.addAll(response.body().getResults());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<MoviesResponse> call, Throwable t) {
                //entreprisesRecycler.invokeState(EmptyStateRecyclerView.STATE_ERROR);
                Log.d("onFailure", t.toString());
            }
        });

    }

    private void getFilteredMovies(String query){
        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<MoviesResponse> call = service.searchMovies(apiKey, query);

        call.enqueue(new Callback<MoviesResponse>() {
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                if(response.body() != null) {
                    movies.clear();
                    movies.addAll(response.body().getResults());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<MoviesResponse> call, Throwable t) {
                //entreprisesRecycler.invokeState(EmptyStateRecyclerView.STATE_ERROR);
                Log.d("onFailure", t.toString());
            }
        });
    }

    private MovieAdapter.OnItemClickListener detailsMovie =new MovieAdapter.OnItemClickListener() {
        @Override
        public void onItemClickDetails(View v, Movie item) {
            showMovieDetails(item);
        }
    };

    private void showMovieDetails(Movie m){
        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<Movie> call = service.getMovieDetails(m.getId(), apiKey);
        call.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                if(response.body() != null) {
                    title.setText(response.body().getTitle());
                    budget.setText(response.body().getBudget().toString());
                    vote.setText(response.body().getVoteAverage().toString());
                    String poster_url = "https://image.tmdb.org/t/p/w500" + response.body().getPosterPath();
                    if (response.body().getPosterPath() == null || response.body().getPosterPath().equals("")) {
                        Glide.with(getApplicationContext()).load(R.drawable.movie_poster).into(poster);
                    } else {
                        Glide.with(getApplicationContext()).load(poster_url).into(poster);
                    }
                    details.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu) {
        getMenuInflater().inflate( R.menu.search, menu);

        final MenuItem myActionMenuItem = menu.findItem( R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getFilteredMovies(query);
                return true;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                if(s == null || s.equals("")){
                    getMoviesItems();
                }
                return false;
            }
        });
        return true;
    }
}
