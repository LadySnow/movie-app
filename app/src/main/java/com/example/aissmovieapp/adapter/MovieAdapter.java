package com.example.aissmovieapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.aissmovieapp.R;
import com.example.aissmovieapp.model.Movie;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder>{

    private Context context;
    private List<Movie> movieList = new ArrayList<>();
    private OnItemClickListener listener;


    public MovieAdapter(List<Movie> movieList, OnItemClickListener listener){
        this.movieList = movieList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.movie_item, viewGroup, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder movieViewHolder, int i) {
        final Movie item = movieList.get(i);
        if(item != null) {
            movieViewHolder.title.setText(item.getTitle());
            if (item.getPosterPath() != null) {
                String poster = "https://image.tmdb.org/t/p/w500" + movieList.get(i).getPosterPath();
                Glide.with(movieViewHolder.poster.getContext()).load(poster).into(movieViewHolder.poster);
            }
        }
        movieViewHolder.c_movie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClickDetails(view, item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public interface OnItemClickListener {
        void onItemClickDetails(View v, Movie item);
    }

    static class MovieViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.cardview_movie)
        CardView c_movie;
        @BindView(R.id.rlMovie)
        RelativeLayout rlmovie;
        @BindView(R.id.movie_poster)
        ImageView poster;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.more)
        TextView more;


        public MovieViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
