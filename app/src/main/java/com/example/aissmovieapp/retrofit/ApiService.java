package com.example.aissmovieapp.retrofit;

import com.example.aissmovieapp.model.Movie;
import com.example.aissmovieapp.model.MoviesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    @GET("movie/popular")
    Call<MoviesResponse> getMovies(
        @Query("api_key") String apiKey
    );

    @GET("search/movie")
    Call<MoviesResponse> searchMovies(
        @Query("api_key") String apiKey,
        @Query("query") String query
    );

    @GET("movie/{movie_id}")
    Call<Movie> getMovieDetails(
        @Path("movie_id") int movie_id,
        @Query("api_key") String apiKey
    );
}
